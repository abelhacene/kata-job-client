import React, { useEffect, useState } from 'react';
import { Typography, Box } from '@mui/material';
import MissionCard from '../components/MissionCard';
import { getAllMissions } from '../services/Missions';
import { Mission } from '../types/Missions';

const MissionsPannel = () => {
  const [missions, setMissions] = useState<Mission[]>([]);

  const fetchMissions = async () => {
    const fetchedMissions = await getAllMissions();

    setMissions(fetchedMissions);
  };

  useEffect(() => {
    if (!missions.length) {
      fetchMissions();
    }
  }, []);

  return (
    <>
      <Typography variant="h3" align="center">
        Missions pannel
      </Typography>
      <Box
        sx={{
          p: 1,
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'wrap',
          gap: '1rem',
        }}
      >
        {missions.length > 0 &&
          missions.map((mission) => (
            <MissionCard mission={mission} key={mission.id} />
          ))}
      </Box>
    </>
  );
};

export default MissionsPannel;
