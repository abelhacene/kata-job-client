import { useState, useEffect } from 'react';
import { FormikConfig, useFormik } from 'formik';
import { getOneMission, updateMission } from '../services/Missions';
import {
  Box,
  TextField,
  Button,
  Snackbar,
  Alert,
  Typography,
} from '@mui/material';
import { useParams } from 'react-router-dom';
import { Mission } from '../types/Missions';

interface FormValues {
  profil: string | undefined;
  client: string | undefined;
  address: string | undefined;
  project: string | undefined;
  duration: string | undefined;
  description: string | undefined;
  stack: string | undefined;
  team_organisation: string | undefined;
}

const UpdateMissionForm = () => {
  const [open, setOpen] = useState(false);
  const [data, setData] = useState<Mission>();
  const { id } = useParams();
  useEffect(() => {
    const findMissionById = async () => {
      if (id) {
        const response = await getOneMission(id);
        setData(response);
        console.log(response); //
      }
    };
    findMissionById();
  }, []);

  const initialValues = {
    profil: data && data.profil,
    client: data && data.client,
    address: data && data.address,
    project: data && data.project,
    duration: data && data.duration,
    description: data && data.description,
    stack: data && data.stack,
    team_organisation: data && data.team_organisation,
  };
  const formik = useFormik({
    initialValues,
    onSubmit: (mission: FormValues) => {
      if (id) {
        updateMission(id, mission);
      }
      setOpen(true);
    },
  });

  return (
    <>
      <Typography variant="h3" align="center">
        Update Mission Offer
      </Typography>
      <form onSubmit={formik.handleSubmit}>
        <Box
          sx={{
            width: '60%',
            margin: 'auto',
            display: 'flex',
            flexDirection: 'column',
            gap: '1rem',
          }}
        >
          <TextField
            id="profil"
            name="profil"
            placeholder={data && data.profil}
            value={formik.values.profil}
            onChange={formik.handleChange}
          />

          <TextField
            id="client"
            name="client"
            placeholder={data && data.client}
            value={formik.values.client}
            onChange={formik.handleChange}
          />

          <TextField
            id="description"
            name="description"
            placeholder={data && data.description}
            value={formik.values.description}
            onChange={formik.handleChange}
          />

          <TextField
            id="address"
            name="address"
            placeholder={data && data.address}
            value={formik.values.address}
            onChange={formik.handleChange}
          />

          <TextField
            id="stack"
            name="stack"
            placeholder={data && data.stack}
            value={formik.values.stack}
            onChange={formik.handleChange}
          />

          <TextField
            id="project"
            name="project"
            placeholder={data && data.project}
            value={formik.values.project}
            onChange={formik.handleChange}
          />

          <TextField
            id="duration"
            name="duration"
            placeholder={data && data.duration}
            value={formik.values.duration}
            onChange={formik.handleChange}
          />

          <TextField
            id="team_organisation"
            name="team_organisation"
            placeholder={data && data.team_organisation}
            value={formik.values.team_organisation}
            onChange={formik.handleChange}
          />
          <Button variant="contained" type="submit">
            Submit
          </Button>
        </Box>
      </form>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={() => {
          setOpen(false);
        }}
      >
        <Alert
          onClose={() => {
            setOpen(false);
          }}
          severity="success"
          sx={{ width: '100%' }}
        >
          Mission Updated Successfully
        </Alert>
      </Snackbar>
    </>
  );
};

export default UpdateMissionForm;
