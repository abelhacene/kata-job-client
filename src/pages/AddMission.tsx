import React, { useState } from 'react';
import { useFormik } from 'formik';
import { postMission } from '../services/Missions';
import {
  Box,
  TextField,
  Button,
  Snackbar,
  Alert,
  Typography,
} from '@mui/material';

interface FormValues {
  profil: string;
  client: string;
  address: string;
  project: string;
  duration: string;
  description: string;
  stack: string;
  team_organisation: string;
}

const initialValues = {
  profil: '',
  client: '',
  address: '',
  project: '',
  duration: '',
  description: '',
  stack: '',
  team_organisation: '',
};

const AddMissionForm = () => {
  const formik = useFormik({
    initialValues,
    onSubmit: (values: FormValues) => {
      postMission(values);
      setOpen(true);
    },
  });

  const [open, setOpen] = useState(false);

  return (
    <>
      <Typography variant="h3" align="center">
        Add Mission Offer
      </Typography>
      <form onSubmit={formik.handleSubmit}>
        <Box
          sx={{
            width: '60%',
            margin: 'auto',
            display: 'flex',
            flexDirection: 'column',
            gap: '1rem',
          }}
        >
          <TextField
            id="profil"
            name="profil"
            label="profil"
            value={formik.values.profil}
            onChange={formik.handleChange}
          />

          <TextField
            id="stack"
            name="client"
            label="client"
            value={formik.values.client}
            onChange={formik.handleChange}
          />

          <TextField
            id="description"
            name="description"
            label="Description"
            value={formik.values.description}
            onChange={formik.handleChange}
          />

          <TextField
            id="address"
            name="address"
            label="Address"
            value={formik.values.address}
            onChange={formik.handleChange}
          />

          <TextField
            id="stack"
            name="stack"
            label="stack"
            value={formik.values.stack}
            onChange={formik.handleChange}
          />

          <TextField
            id="project"
            name="project"
            label="project"
            value={formik.values.project}
            onChange={formik.handleChange}
          />
          <TextField
            id="duration"
            name="duration"
            label="duration"
            value={formik.values.duration}
            onChange={formik.handleChange}
          />

          <TextField
            id="team_organisation"
            name="team_organisation"
            label="team organisation"
            value={formik.values.team_organisation}
            onChange={formik.handleChange}
          />

          <Button variant="contained" type="submit">
            Submit
          </Button>
        </Box>
      </form>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={() => {
          setOpen(false);
        }}
      >
        <Alert
          onClose={() => {
            setOpen(false);
          }}
          severity="success"
          sx={{ width: '100%' }}
        >
          Mission Saved Successfully
        </Alert>
      </Snackbar>
    </>
  );
};

export default AddMissionForm;
