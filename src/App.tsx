import React from 'react';
import AppRouter from './AppRouter';
import Nav from './components/navigation/Nav';
import { BrowserRouter } from 'react-router-dom';
import './App.css';

function App() {
  return (
    <>
      <BrowserRouter>
        <Nav />
        <AppRouter />
      </BrowserRouter>
    </>
  );
}

export default App;
