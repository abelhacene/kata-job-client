export type Mission = {
  id?: string | undefined;
  profil: string | undefined;
  client: string | undefined;
  address: string | undefined;
  project: string | undefined;
  duration: string | undefined;
  description: string | undefined;
  stack: string | undefined;
  team_organisation: string | undefined;
  created_at?: Date | undefined;
  updated_at?: Date | undefined;
};
