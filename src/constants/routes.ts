export const PAGE_HOME = '/';
export const PAGE_NEWMISSION = '/add';
export const PAGE_NEWMISSION_FULLPATH = '/missions/add';
export const PAGE_MISSIONSPANNEL = '/';
export const PAGE_MISSIONSPANNEL_FULLPATH = '/missions/';
