import React from 'react';
import moment from 'moment';
import { useNavigate } from 'react-router-dom';
import {
  Work as WorkIcon,
  Delete as DeleteIcon,
  BorderColor as Pen,
} from '@mui/icons-material';
import { Card, CardContent, Typography, Button } from '@mui/material';

import { Mission } from '../types/Missions';
import { deleteMission } from '../services/Missions';

const MissionCard = ({ mission: mission }: { mission: Mission }) => {
  const {
    id,
    profil,
    client,
    address,
    project,
    duration,
    description,
    stack,
    team_organisation,
    created_at,
    updated_at,
  } = mission;

  const url = `/missions/${mission.id}`;
  const navigate = useNavigate();

  const handleNavigation = () => {
    navigate(url);
  };

  return (
    <Card sx={{ width: '30%' }}>
      <CardContent>
        <Typography variant="h4">{profil}</Typography>
        <Typography>
          {client} .{address}
        </Typography>
        <Typography>{project}</Typography>
        <Typography>
          <WorkIcon /> {stack}
        </Typography>

        <Typography>{description}</Typography>
        <Typography> {moment(created_at).format('YYYY - MM - DD')}</Typography>
        {id && (
          <Button color="error">
            <DeleteIcon
              onClick={() => {
                deleteMission(id);
                window.location.reload();
              }}
            />
          </Button>
        )}
        <Button>
          <Pen onClick={handleNavigation} />
        </Button>
      </CardContent>
    </Card>
  );
};

export default MissionCard;
