import React from 'react';
import { Link } from 'react-router-dom';
import { Typography, Box } from '@mui/material';

import {
  PAGE_HOME,
  PAGE_MISSIONSPANNEL_FULLPATH,
  PAGE_NEWMISSION_FULLPATH,
} from '../../constants/routes';

const Nav = () => {
  return (
    <Box
      sx={{
        display: 'flex',
        p: 1,
        gap: '1rem',
        backgroundColor: 'rgb(25, 118, 210)',
      }}
    >
      <Link
        to={PAGE_HOME}
        style={{
          textDecoration: 'none',
          color: 'white',
        }}
      >
        <Typography>Home</Typography>
      </Link>
      <Link
        to={PAGE_MISSIONSPANNEL_FULLPATH}
        style={{
          textDecoration: 'none',
          color: 'white',
        }}
      >
        <Typography>Missions</Typography>
      </Link>
      <Link
        to={PAGE_NEWMISSION_FULLPATH}
        style={{
          textDecoration: 'none',
          color: 'white',
        }}
      >
        <Typography>Add Mission</Typography>
      </Link>
    </Box>
  );
};

export default Nav;
