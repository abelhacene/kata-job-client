import React from 'react';
import { Route, Routes } from 'react-router-dom';
import {
  PAGE_HOME,
  PAGE_MISSIONSPANNEL_FULLPATH,
  PAGE_NEWMISSION_FULLPATH,
} from './constants/routes';
import Home from './pages/Home';
import AddMission from './pages/AddMission';
import MissionsPannel from './pages/MissionsPannel';
import UpdateMissionForm from './pages/UpdateMission';

export default function AppRouter() {
  return (
    <>
      <Routes>
        <Route path={PAGE_HOME} element={<Home />} />
        <Route path="/missions">
          <Route
            path={PAGE_MISSIONSPANNEL_FULLPATH}
            element={<MissionsPannel />}
          />
          <Route path={PAGE_NEWMISSION_FULLPATH} element={<AddMission />} />
          <Route path="/missions/:id" element={<UpdateMissionForm />} />
        </Route>
      </Routes>
    </>
  );
}
