import { Mission } from '../types/Missions';
import axios from './axios';

export const getAllMissions = async (): Promise<Mission[]> => {
  const { data } = await axios.get('/missions');

  return data;
};
export const getOneMission = async (MissionId: string): Promise<Mission> => {
  const { data } = await axios.get(`/missions/${MissionId}`);

  return data;
};

export const postMission = async (mission: Mission): Promise<string> => {
  const { data } = await axios.post(`/missions`, mission);
  return data;
};

export const deleteMission = async (missionId: string): Promise<string> => {
  const { data } = await axios.delete(`/missions/${missionId}`);
  return data;
};

export const updateMission = async (
  missionId: string,
  mission: Mission,
): Promise<string> => {
  const { data } = await axios.patch(`/missions/${missionId}`, mission);
  return data;
};
